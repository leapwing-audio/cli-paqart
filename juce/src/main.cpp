/*!
 * Paqart: Plugin Audio Quality Assurance and Regression Testing tool
 * Copyright (C) 2018  Leapwing Audio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "app/Options.hpp"
#include "app/gui/MainWindow.hpp"
#include "app/headless/run.hpp"
#include "lw/mss.hpp"
#include "lw/naft/write/Document.hpp"

class Application : public juce::JUCEApplication
{
public:
    void initialise(const String &commandline) override
    {
        auto array = getCommandLineParameterArray();
        std::vector<std::string> args(1 + array.size());
        args[0] = "paqart";
        for (unsigned int i = 0; i < array.size(); ++i)
            args[1 + i] = array[i].toStdString();

        if (!parse_(args) || !run_())
        {
            setApplicationReturnValue(-1);
            quit();
        }
    }

    void shutdown() override
    {
        window_.reset();
    }

    const String getApplicationName() override
    {
        return "Paqart";
    }
    const String getApplicationVersion() override
    {
        return "1";
    }

public:
    bool run_()
    {
        MSS_BEGIN(bool);

        switch (options_.mode)
        {
            case app::Options::Mode::Headless:
                window_ = std::make_unique<app::gui::MainWindow>(false);
                MSS(app::headless::run(options_));
                quit();
                break;

            case app::Options::Mode::Gui:
                window_ = std::make_unique<app::gui::MainWindow>();
                window_->run(options_);
                break;

            default:
                MSS(false);
                break;
        }

        MSS_END();
    }

    bool parse_(const std::vector<std::string> &arguments)
    {
        MSS_BEGIN(bool);

        const unsigned int num_args = arguments.size();

        std::vector<const char *> argv(num_args);
        for (unsigned int i = 0; i < num_args; ++i)
            argv[i] = arguments[i].data();

        const auto result = options_.parse(num_args, argv.data());
        switch (result.status)
        {
            case lw::argo::ReturnCode::Error:
                std::cerr << "Error: " << result.message << std::endl;
                MSS(options_.help(std::cout));
                MSS(false);
                break;

            case lw::argo::ReturnCode::SuccessAndAbort:
                MSS(false);
                break;

            default:
                break;
        }

        MSS_END();
    }

    std::unique_ptr<app::gui::MainWindow> window_;
    app::Options options_;
};

START_JUCE_APPLICATION(Application);
