#ifndef HEADER_app_PluginLoader_hpp_ALREADY_INCLUDED
#define HEADER_app_PluginLoader_hpp_ALREADY_INCLUDED

/*!
 * Paqart: Plugin Audio Quality Assurance and Regression Testing tool
 * Copyright (C) 2018  Leapwing Audio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <JuceHeader.h>
#include <lw/file/Path.hpp>
#include <lw/file/write.hpp>
#include <lw/mss.hpp>
#include <lw/naft/write/Document.hpp>
#include <map>

namespace app {

    class PluginLoader
    {
    public:
        using Description = juce::PluginDescription;
        using PluginPtr = std::shared_ptr<juce::AudioPluginInstance>;

        PluginLoader()
            : desc_()
        {
        }

        bool initialize(const std::string &filename)
        {
            MSS_BEGIN(bool);

            MSS(!filename.empty(), std::cerr << "No plugin filename supplied" << std::endl);

            mngr_.addDefaultFormats();

            // scan the file
            {
                for (auto *fmt : mngr_.getFormats())
                {
                    juce::OwnedArray<juce::PluginDescription> desc_array;
                    known_plugins_.scanAndAddFile(filename, false, desc_array, *fmt);

                    // we manage them ourselfs
                    for (unsigned int i = 0; i < desc_array.size(); ++i)
                        all_plugins_.emplace_back(*desc_array[i], fmt);
                }

                // check that a plugin is found
                if (all_plugins_.empty())
                {
                    std::cerr << "No plugin detected in '" << filename << "'" << std::endl;
                    std::cerr << "I tried the following types:";
                    for (unsigned int i = 0; i < mngr_.getNumFormats(); ++i)
                        std::cerr << " " << mngr_.getFormat(i)->getName();
                    MSS(false);
                }
            }

            MSS_END();
        }

        PluginPtr create(const std::string &name, unsigned int sample_rate, unsigned int frame_size)
        {
            auto with_same_name = [&](const auto &p) {
                return (all_plugins_.size() == 1 && name.empty()) || p.first.name.toStdString() == name;
            };

            auto it = std::find_if(all_plugins_.begin(), all_plugins_.end(), with_same_name);
            if (it == all_plugins_.end())
            {
                std::cerr << "Could not find plugin with name '" << name << "'" << std::endl;
                return nullptr;
            }

            // create the plugin
            juce::AudioPluginFormat *fmt = it->second;
            juce::PluginDescription desc = it->first;

            juce::String error;
            PluginPtr p = mngr_.createPluginInstance(desc, sample_rate, frame_size, error);

            if (!p)
                std::cerr << "Unable to create plugin '" << error << "'" << std::endl;

            return p;
        }

        static void save_state(PluginPtr &ptr, const juce::File &file)
        {
            if (!ptr)
                return;

            // save the state
            juce::MemoryBlock dst;
            ptr->getStateInformation(dst);

            juce::FileOutputStream str(file);
            str.setPosition(0);
            str.truncate();

            str.write(dst.getData(), dst.getSize());

            // save the state as naft
            {
                lw::file::Path fn = file.getFileName().toStdString() + ".naft";
                std::ofstream ofs;
                if (!lw::file::open(ofs, fn))
                    return;

                lw::naft::write::Document doc(ofs);
                {
                    auto n = doc.node("plugin info");
                    const auto &params = ptr->getParameters();
                    for (unsigned int idx = 0; idx < params.size(); ++idx)
                    {
                        auto param = params[idx];
                        n.node("parameter").attr("idx", idx).attr("name", param->getName(-1)).attr("value", param->getValue());
                    }
                }
            }
        }

        static void load_state(PluginPtr &ptr, const juce::File &file)
        {
            if (!ptr)
                return;

            juce::FileInputStream str(file);

            juce::MemoryBlock dst(str.getTotalLength());
            str.read(dst.getData(), dst.getSize());

            ptr->setStateInformation(dst.getData(), dst.getSize());
        }

        const std::vector<std::pair<Description, juce::AudioPluginFormat *>> &all_plugins() const
        {
            return all_plugins_;
        }

    private:
        std::vector<std::pair<Description, juce::AudioPluginFormat *>> all_plugins_;
        juce::KnownPluginList known_plugins_;
        juce::AudioPluginFormatManager mngr_;
        Description desc_;
    };

}

#endif
