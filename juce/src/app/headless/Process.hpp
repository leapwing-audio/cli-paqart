#ifndef HEADER_app_headless_Process_hpp_ALREADY_INCLUDED
#define HEADER_app_headless_Process_hpp_ALREADY_INCLUDED

/*!
 * Paqart: Plugin Audio Quality Assurance and Regression Testing tool
 * Copyright (C) 2018  Leapwing Audio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "JuceHeader.h"
#include "app/Options.hpp"
#include "app/headless/Scenario.hpp"
#include "app/PluginLoader.hpp"
#include "lw/naft/write/Node.hpp"

namespace app { namespace headless {

    struct Process : public PluginLoader
    {
        Process(const Options &options);

        bool initialize();
        bool operator()();

    private:
        using Node = lw::naft::write::Node;
        using NodePtr = std::unique_ptr<Node>;

        bool initialize_plugin_(NodePtr &node);
        bool initialize_scenario_(NodePtr &node);
        bool read_wav_(NodePtr &node);
        bool write_wav_(NodePtr &node);

        bool extract_plugin_info_(NodePtr &node);
        bool process_scenario_(NodePtr &node);
        bool update_parameters_(const std::vector<Parameter> &new_parameters, NodePtr &node);
        juce::AudioProcessorParameter *find_parameter_(const Parameter &parameter);
        bool write_timings_(NodePtr &node);
        bool process_block_(juce::AudioSampleBuffer &buffer, unsigned int sample_index);
        bool setup_buses_(NodePtr &node);
        NodePtr make_root_node_(const std::string &name) const;

        NodePtr make_node_(NodePtr &node, const std::string &name, unsigned int verbosity = 1) const;

        template<typename CB>
        NodePtr make_node_(NodePtr &node, const std::string &name, CB &&cb) const
        {
            return make_node_(node, name, 1, cb);
        }
        template<typename CB>
        NodePtr make_node_(NodePtr &node, const std::string &name, unsigned int verbosity, CB &&cb) const
        {
            NodePtr child = make_node_(node, name, verbosity);
            if (child)
                cb(*child);

            return child;
        }

        Options options_;
        juce::FileLogger logger_;

        Scenario scenario_;
        juce::AudioSampleBuffer input_;
        juce::AudioSampleBuffer output_;
        PluginPtr plugin_;

        double sample_rate_;
        unsigned int frame_size_ = 256;

        struct FrameTimeInfo
        {
            unsigned int size;
            double time;
        };
        std::vector<FrameTimeInfo> time_info_;
        unsigned int latency_;
    };
}}

#endif

