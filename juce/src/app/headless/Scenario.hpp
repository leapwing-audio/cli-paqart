#ifndef HEADER_app_headless_Scenario_hpp_ALREADY_INCLUDED
#define HEADER_app_headless_Scenario_hpp_ALREADY_INCLUDED

/*!
 * Paqart: Plugin Audio Quality Assurance and Regression Testing tool
 * Copyright (C) 2018  Leapwing Audio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <istream>
#include <limits>
#include <vector>

namespace app { namespace headless {

    struct Parameter
    {
        static unsigned int invalid_idx() { return std::numeric_limits<unsigned int>::max(); }

        std::string name;
        unsigned int idx = invalid_idx();
        float value = 0;
    };

    struct Frame
    {
        unsigned int frame_size = 0;
        bool halt = false;
        std::vector<Parameter> parameters;
    };

    struct IndexedFrame : public Frame
    {
        unsigned int index = 0;
    };

    struct Scenario
    {
        std::vector<std::string> output_layouts;
        std::string input_layout;
        Frame initial;
        unsigned int latency = std::numeric_limits<unsigned int>::max();
        std::vector<IndexedFrame> frames;
    };

    bool load_scenario(std::istream &iss, Scenario &scenario);
}}

#endif
