#ifndef HEADER_app_headless_run_hpp_ALREADY_INCLUDED
#define HEADER_app_headless_run_hpp_ALREADY_INCLUDED

/*!
 * Paqart: Plugin Audio Quality Assurance and Regression Testing tool
 * Copyright (C) 2018  Leapwing Audio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "app/headless/Process.hpp"

namespace app { namespace headless {

    struct P : public juce::Thread
    {
        P(const std::function<bool()> &func)
            : juce::Thread("A")
            , func_(func)
        {}

        void run() override
        {
            result = func_();
        }
        std::function<bool()> func_;
        std::atomic<bool> result{ false };
    };

    inline bool run(const Options &options)
    {
        MSS_BEGIN(bool);
        app::headless::Process process(options);
        MSS(process.initialize());


        P p([&]() { return process(); });

        p.startThread();
        p.waitForThreadToExit(-1);
        MSS(p.result.load());

        MSS_END();
    }
}}

#endif
