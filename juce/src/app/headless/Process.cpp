/*!
 * Paqart: Plugin Audio Quality Assurance and Regression Testing tool
 * Copyright (C) 2018  Leapwing Audio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "app/headless/Process.hpp"
#include <cctype>
#include <chrono>
#include <fstream>
#include <map>
#include <memory>
#include "lw/naft/write/Document.hpp"

#define PARAMETER_MAX_NAME_LENGTH 256

namespace app { namespace headless {

    Process::Process(const Options &options)
        : options_(options)
        , logger_(juce::File::getCurrentWorkingDirectory().getChildFile("log.txt"), "Leapwing paqart log")
    {
    }

    Process::NodePtr Process::make_root_node_(const std::string &name) const
    {
        if (options_.verbosity > 0)
            return std::make_unique<Node>(lw::naft::write::Document(std::cout).node(name));
        else
            return std::unique_ptr<Node>();
    };

    Process::NodePtr Process::make_node_(NodePtr &node, const std::string &name, unsigned int verbosity) const
    {
        if (!node)
            return NodePtr();

        if (verbosity > options_.verbosity)
            return NodePtr();

        return std::make_unique<Node>(node->node(name));
    }

    bool Process::operator()()
    {
        MSS_BEGIN(bool);
        MSS(!!plugin_);

        {
            auto n = make_root_node_("processing");

            MSS(extract_plugin_info_(n));
            MSS(process_scenario_(n));
            MSS(write_wav_(n));
        }
        {
            auto n = make_root_node_("timings");
            MSS(write_timings_(n));
        }

        MSS_END();
    }

    bool Process::write_timings_(NodePtr &node)
    {
        MSS_BEGIN(bool);

        unsigned int total_num_samples = 0;
        double total_time = 0;

        {
            auto n = make_node_(node, "frames");
            for (const auto &info : time_info_)
            {
                make_node_(n, "frame", 2, [&](auto &n) { n.attr("size", info.size).attr("time", info.time); });

                total_num_samples += info.size;
                total_time += info.time;
            }
        }

        double audio = total_num_samples / sample_rate_;

        make_node_(node, "audio", [&](auto &n) { n.attr(audio); });
        make_node_(node, "processing", [&](auto &n) { n.attr(total_time); });
        make_node_(node, "factor", [&](auto &n) { n.attr(audio / total_time); });

        MSS_END();
    }

    bool Process::process_block_(juce::AudioSampleBuffer &buffer, unsigned int sample_index)
    {
        MSS_BEGIN(bool);

        unsigned int framesize = buffer.getNumSamples();

        // actual processing
        juce::MidiBuffer msgs;
        double duration = 0;
        {
            auto start = std::chrono::system_clock::now();
            plugin_->processBlock(buffer, msgs);
            auto end = std::chrono::system_clock::now();

            std::chrono::duration<double> diff = end - start;
            duration = diff.count();
        }

        // store in the timing
        time_info_.push_back({ framesize, duration });

        // write the output
        for (unsigned int i = 0; i < output_.getNumChannels(); ++i)
            std::copy_n(buffer.getReadPointer(i, 0), framesize, output_.getWritePointer(i, sample_index));

        MSS_END();
    }

    namespace {

        struct FrameProcessInfo
        {
            unsigned int sample_index;
            unsigned int frame_index;
            unsigned int frame_size;
        };

        void write_input_in_buffer(juce::AudioSampleBuffer &tgt, const juce::AudioSampleBuffer &input, const FrameProcessInfo &info)
        {
            unsigned int from_input = 0;

            // can we take samples from the input
            if (input.getNumSamples() > info.sample_index)
                from_input = std::min(info.frame_size, input.getNumSamples() - info.sample_index);

            // the number of zero's we need to add to the end
            unsigned int end_zeros = info.frame_size - from_input;

            // fill tgt with zero's
            for (unsigned int i = 0; i < input.getNumChannels(); ++i)
            {
                if (from_input > 0)
                    std::copy_n(input.getReadPointer(i, info.sample_index), from_input, tgt.getWritePointer(i, 0));

                if (end_zeros > 0)
                    std::fill_n(tgt.getWritePointer(i, from_input), end_zeros, 0);
            }
        }

        bool compare_params(const std::string &lhs, const std::string &rhs)
        {
            auto lower_no_space = [](const std::string &str) {
                std::string res;
                for (unsigned int i = 0; i < str.size(); ++i)
                    if (str[i] != ' ')
                        res += std::tolower(str[i]);
                return res;
            };

            return lower_no_space(lhs) == lower_no_space(rhs);
        }

    }

    bool Process::process_scenario_(NodePtr &node)
    {
        MSS_BEGIN(bool);

        // get the total size base on the input
        unsigned int total_size = input_.getNumSamples();
        const unsigned int total_num_output_channels = plugin_->getTotalNumOutputChannels();

        // reserve an output buffer
        output_ = juce::AudioSampleBuffer(total_num_output_channels, total_size);

        FrameProcessInfo info;
        info.sample_index = 0;
        info.frame_index = 0;
        info.frame_size = scenario_.initial.frame_size;

        auto frame_desc_it = scenario_.frames.begin();

        auto n = make_node_(node, "frames", 2u);

        // as long as there are samples to process
        while (info.sample_index < total_size)
        {
            // update the frame description iterator
            if (frame_desc_it != scenario_.frames.end() && frame_desc_it->index == info.frame_index)
            {
                // set the framesize (if set)
                auto &frame = *frame_desc_it;
                if (frame.halt)
                    MSS_RETURN_OK();

                auto nn = make_node_(n, "update", [&](auto &n) {
                    n.attr("index", frame.index);
                    n.attr("frame_size", frame.frame_size);
                });

                if (frame.frame_size != 0)
                    info.frame_size = frame.frame_size;

                // update the parameters
                MSS(update_parameters_(frame.parameters, nn));

                // and go to the next description
                ++frame_desc_it;
            }

            // can be less at the end
            info.frame_size = std::min<unsigned int>(total_size - info.sample_index, info.frame_size);
            MSS(info.frame_size > 0);

            // output some infor,ation
            make_node_(n, "frame", [&](auto &n) {
                n.attr("index", info.frame_index).attr("start", info.sample_index).attr("end", info.sample_index + info.frame_size).attr("size", info.frame_size);
            });

            // reserve a buffer for the input output
            juce::AudioSampleBuffer buffer(std::max<unsigned int>(input_.getNumChannels(), total_num_output_channels), info.frame_size);

            // write the input (if available)
            write_input_in_buffer(buffer, input_, info);

            MSS(process_block_(buffer, info.sample_index));

            // and time for the next frame
            info.sample_index += info.frame_size;
            ++info.frame_index;
        }

        MSS_END();
    }

    juce::AudioProcessorParameter *Process::find_parameter_(const Parameter &parameter)
    {
        const auto &params = plugin_->getParameters();

        // name set, try to find by name
        if (!parameter.name.empty())
        {
            for (auto *p : params)
                if (compare_params(p->getName(PARAMETER_MAX_NAME_LENGTH).toStdString(), parameter.name))
                    return p;

            std::cerr << "Invalid parameter name '" << parameter.name << "': no such parameter is known" << std::endl;
            return nullptr;
        }
        else
        {
            if (parameter.idx < params.size())
                return params[parameter.idx];

            std::cerr << "Invalid parameter idx '" << parameter.idx << "': out of bounds" << std::endl;
            return nullptr;
        }
    }

    bool Process::update_parameters_(const std::vector<Parameter> &new_parameters, NodePtr &node)
    {
        MSS_BEGIN(bool);

        const auto &params = plugin_->getParameters();

        for (const Parameter &param : new_parameters)
        {
            auto *ptr = find_parameter_(param);
            MSS(!!ptr);
            ptr->setValueNotifyingHost(param.value);
            make_node_(node, "parameter", [&](auto &n) {
                n.attr("idx", ptr->getParameterIndex()).attr("name", ptr->getName(PARAMETER_MAX_NAME_LENGTH)).attr("given_value", param.value).attr("actual_value", ptr->getValue());
            });
        }

        MSS_END();
    }

    bool Process::extract_plugin_info_(NodePtr &node)
    {
        MSS_BEGIN(bool);

        MSS(!!plugin_);

        {
            auto n = make_node_(node, "initial plugin setting");
            // set the parameters from the initial frame
            MSS(update_parameters_(scenario_.initial.parameters, n));
        }

        auto n = make_node_(node, "plugin info");

        // the parameters
        {
            const auto &params = plugin_->getParameters();
            for (unsigned int i = 0; i < params.size(); ++i)
            {
                auto *p = params[i];
                make_node_(n, "parameter", [&](auto &n) { n.attr("idx", i).attr("name", p->getName(PARAMETER_MAX_NAME_LENGTH)).attr("value", p->getValue()); });
            }
        }

        // the latency
        {
            int latency = plugin_->getLatencySamples();
            make_node_(n, "latency", [&](auto &n) {
                n.attr("value", latency);

                if (scenario_.latency != std::numeric_limits<unsigned int>::max())
                {
                    n.attr("overwritten", scenario_.latency);
                    latency = scenario_.latency;
                }
            });

            MSS(latency >= 0);
            latency_ = latency;
        }

        //

        MSS_END();
    }

    bool Process::initialize()
    {
        MSS_BEGIN(bool);

        auto n = make_root_node_("initialization");

        MSS(initialize_scenario_(n));
        MSS(read_wav_(n));
        MSS(initialize_plugin_(n));

        MSS_END();
    }
    bool Process::write_wav_(NodePtr &node)
    {
        MSS_BEGIN(bool);
        juce::WavAudioFormat fmt;

        juce::File outputFile(options_.output_filename);
        MSS(outputFile.deleteFile());

        auto ptr = new juce::FileOutputStream(outputFile);
        MSS(ptr && ptr->openedOk(), delete ptr; std::cerr << "Cannot get a stream to '" << options_.output_filename << "'" << std::endl);

        std::unique_ptr<juce::AudioFormatWriter> writer(fmt.createWriterFor(ptr, sample_rate_, output_.getNumChannels(), 24, NULL, 0));
        MSS(writer->writeFromAudioSampleBuffer(output_, 0, output_.getNumSamples()), std::cerr << "Error writing file '" << options_.output_filename << "'");

        MSS_END();
    }

    bool Process::read_wav_(NodePtr &node)
    {
        MSS_BEGIN(bool);
        juce::AudioFormatManager formatManager;
        formatManager.registerBasicFormats();

        juce::AudioFormat *audioFormat = formatManager.getDefaultFormat();

        juce::File f;
        if (juce::File::isAbsolutePath(options_.input_file))
            f = juce::File(options_.input_file);
        else
            f = juce::File::getCurrentWorkingDirectory().getChildFile(options_.input_file);

        std::unique_ptr<juce::AudioFormatReader> reader(formatManager.createReaderFor(f));
        MSS(!!reader, std::cerr << "Unable to create a reader for file '" << options_.input_file << "'" << std::endl);

        input_ = juce::AudioSampleBuffer(reader->numChannels, reader->lengthInSamples);
        reader->read(&input_, 0, reader->lengthInSamples, 0, true, true);

        make_node_(node, "input", [&](auto &n) {
            n.attr("file", options_.input_file)
                .attr("num_channels", reader->numChannels)
                .attr("num_samples", reader->lengthInSamples)
                .attr("sample_rate", reader->sampleRate);
        });

        sample_rate_ = reader->sampleRate;
        MSS_END();
    }

    bool Process::initialize_scenario_(NodePtr &node)
    {
        MSS_BEGIN(bool);
        std::ifstream ifs;
        ifs.open(options_.description_filename);
        MSS(ifs.good(), std::cerr << "Unable to open description file :'" << options_.description_filename << "'" << std::endl);

        MSS(load_scenario(ifs, scenario_));

        make_node_(node, "scenario", [&](auto &n) {
            n.attr("num_frames", scenario_.frames.size());
            for (auto no : scenario_.output_layouts)
                n.attr("output_layout", no);
            if (!scenario_.input_layout.empty())
                n.attr("input_layout", scenario_.input_layout);
        });

        MSS(scenario_.initial.frame_size > 0, std::cerr << "The initial frame size should not be zero" << std::endl);

        MSS_END();
    }

    bool Process::setup_buses_(NodePtr &node)
    {
        MSS_BEGIN(bool);

        MSS(!!plugin_);

        // we do not support with more than one input bus
        auto layout = plugin_->getBusesLayout();
        MSS(layout.inputBuses.size() == 1, std::cout << "No plugins with more than one input bus are supported" << std::endl);

        // we try to rescale the output so that it match the input in the wav file
        const unsigned int cur_nr_in = plugin_->getMainBusNumInputChannels();
        MSS(cur_nr_in > 0, std::cout << "Expected at least one input channel in the bus" << std::endl);
        const unsigned int req_nr_in = input_.getNumChannels();

        // converts a string to a channel set, can either be a number or a set of channels
        auto make_channel_set = [](const std::string &str) {
            char * end = nullptr;
            auto size = std::strtol(str.data(), &end, 10);

            if (end == str.data() + str.size())
                return juce::AudioChannelSet::canonicalChannelSet(size);
            else
                return juce::AudioChannelSet::fromAbbreviatedString(str);
        };

        if (!scenario_.input_layout.empty())
        {
            auto main_input = make_channel_set(scenario_.input_layout);
            MSS(main_input.size() == req_nr_in, std::cout << "Needs " << req_nr_in << " in channels, but layout '" << scenario_.input_layout << "' has " << main_input.size() << std::endl);

            layout.inputBuses = Array<juce::AudioChannelSet>(main_input);
        }
        else if (req_nr_in != cur_nr_in)
        {
            layout.inputBuses = Array<juce::AudioChannelSet>(juce::AudioChannelSet::canonicalChannelSet(req_nr_in));
        }

        // create an output bus per layout
        if (!scenario_.output_layouts.empty())
        {
            layout.outputBuses.clear();

            for (const auto &d : scenario_.output_layouts)
                layout.outputBuses.add(make_channel_set(d));
        }
        // rescale the existing buses to match the in/out settings
        else if (req_nr_in != cur_nr_in)
        {
            for (auto &set : layout.outputBuses)
                set = juce::AudioChannelSet::canonicalChannelSet(set.size() * req_nr_in / cur_nr_in);
        }

        MSS(plugin_->setBusesLayout(layout), std::cout << "Failed" << std::endl);

        MSS_END();
    }

    bool Process::initialize_plugin_(NodePtr &node)
    {
        MSS_BEGIN(bool);

        juce::Logger::setCurrentLogger(&logger_);

        MSS(PluginLoader::initialize(options_.plugin_filename));
        plugin_ = create(options_.plugin_name, sample_rate_, scenario_.initial.frame_size);
        MSS(!!plugin_);

        if (!options_.plugin_state.empty())
            PluginLoader::load_state(plugin_, juce::File(juce::String(options_.plugin_state)));

        // write info on the plugin(s) that have been found
        make_node_(node, "plugins", [&](auto &n) {
            n.attr("path", options_.plugin_filename);
            for (const auto &p : all_plugins())
            {
                auto nn = n.node("plugin");
                nn.attr("name", p.first.name);
                nn.attr("format", p.second->getName());
                nn.attr("description", p.first.descriptiveName);
                nn.attr("manufacturer", p.first.manufacturerName);
                nn.attr("category", p.first.category);
                nn.attr("version", p.first.version);
                nn.attr("numInputChannels", p.first.numInputChannels);
                nn.attr("numOutputChannels", p.first.numOutputChannels);
                if (p.first.isInstrument) nn.attr("instrument");
            }
        });

        MSS(setup_buses_(node));
        plugin_->prepareToPlay(sample_rate_, scenario_.initial.frame_size);

        MSS_END();
    }
}}
