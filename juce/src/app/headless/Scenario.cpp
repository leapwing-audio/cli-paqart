/*!
 * Paqart: Plugin Audio Quality Assurance and Regression Testing tool
 * Copyright (C) 2018  Leapwing Audio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "app/headless/Scenario.hpp"
#include <iostream>
#include "lw/mss.hpp"
#include "lw/naft/read/Document.hpp"

namespace app { namespace headless {

    using namespace lw::naft::read;

    namespace {

        template<typename T>
        typename T::value_type &append(T &vct)
        {
            return *vct.insert(vct.end(), typename T::value_type());
        }

        bool assign(float &value, const std::string &str, bool *set_flag = nullptr)
        {
            MSS_BEGIN(bool);
            if (set_flag) MSS(!*set_flag);
            try
            {
                value = std::stof(str);
            } catch (...)
            {
                MSS(false, std::cerr << "Unable to convert '" << str << "' to float" << std::endl);
            }

            if (set_flag) *set_flag = true;
            MSS_END();
        }

        bool assign(unsigned int &value, const std::string &str, bool *set_flag = nullptr)
        {
            MSS_BEGIN(bool);
            if (set_flag) MSS(!*set_flag);
            try
            {
                value = std::stoul(str);
            } catch (...)
            {
                MSS(false, std::cerr << "Unable to convert '" << str << "' to unsigned int" << std::endl);
            }

            if (set_flag) *set_flag = true;
            MSS_END();
        }

        bool assign(std::string &value, const std::string &str, bool *set_flag = nullptr)
        {
            MSS_BEGIN(bool);
            if (set_flag) MSS(!*set_flag);
            value = str;

            if (set_flag) *set_flag = true;
            MSS_END();
        }

        bool parse(Parameter &parameter, Document &document)
        {
            MSS_BEGIN(bool);

            bool idx_set = false, value_set = false, name_set = false;
            while (!document.attributes_finished())
            {
                std::string key, value;
                MSS(document.pop_attribute(key, value), std::cerr << "Error geting attributes for Parameter" << std::endl);

                if (false)
                {
                }
                else if (key == "idx")
                    MSS(assign(parameter.idx, value, &idx_set));
                else if (key == "value")
                    MSS(assign(parameter.value, value, &value_set));
                else if (key == "name")
                    MSS(assign(parameter.name, value, &name_set));
                else
                    MSS(false, std::cerr << "Unexpected attribute '" << key << "' for Parameter (idx, name and value are allowed)" << std::endl);
            }

            MSS(idx_set != name_set, std::cerr << "Exactly one of Parameter:(idx) or Parameter:(name) should be set" << std::endl);
            MSS(value_set, std::cerr << "Parameter:(value) is required" << std::endl);

            MSS(!document.child_node_open(), std::cerr << "[Parameter] cannot have child nodes");

            MSS_END();
        }
        bool parse(IndexedFrame &frame, Document &document)
        {
            MSS_BEGIN(bool);

            bool index_set = false;
            while (!document.attributes_finished())
            {
                std::string key, value;
                MSS(document.pop_attribute(key, value), std::cerr << "Error getting attributes for IndexedFrame" << std::endl);
                if (false)
                {
                }
                else if (key == "index")
                    MSS(assign(frame.index, value, &index_set));
                else if (key == "frame_size")
                    MSS(assign(frame.frame_size, value));
                else if (key == "halt")
                    frame.halt = true;
                else
                    MSS(false, std::cerr << "Unknown attribute '" << key << "' for IndexedFrame (index, frame_size, halt are allowed)" << std::endl);
            }

            MSS(index_set, std::cerr << "IndexedFrame:(index) is required" << std::endl);

            if (document.child_node_open())
            {
                while (!document.child_node_close())
                {
                    std::string tag;
                    MSS(document.pop_tag(tag), std::cerr << "Error getting childnodes for IndexedFrame" << std::endl);

                    if (false)
                    {
                    }
                    else if (tag == "parameter")
                        MSS(parse(append(frame.parameters), document));
                    else
                        MSS(false, std::cerr << "Unknown tag '" << tag << "' for IndexedFrame (parameter is allowed)" << std::endl);
                }
            }

            MSS_END();
        }

        bool parse(Frame &frame, Document &document)
        {
            MSS_BEGIN(bool);

            bool frame_size_set = false;
            while (!document.attributes_finished())
            {
                std::string key, value;
                MSS(document.pop_attribute(key, value), std::cerr << "Error getting attributes for Frame" << std::endl);

                if (false)
                {
                }
                else if (key == "frame_size")
                    MSS(assign(frame.frame_size, value, &frame_size_set));
                else
                    MSS(false, std::cerr << "Unknown attribute '" << key << "' for Frame (frame_size is allowed)" << std::endl);
            }

            MSS(frame_size_set, std::cerr << "Frame:(frame_size) is required" << std::endl);

            if (document.child_node_open())
            {
                while (!document.child_node_close())
                {
                    std::string tag;
                    MSS(document.pop_tag(tag), std::cerr << "Error getting tag for Frame" << std::endl);

                    if (false)
                    {
                    }
                    else if (tag == "parameter")
                        MSS(parse(append(frame.parameters), document));
                    else
                        MSS(false, std::cerr << "Unknown tag '" << tag << "' for Frame (tag is allowed)" << std::endl);
                }
            }

            MSS_END();
        }

        bool parse(Scenario &scenario, Document &document)
        {
            MSS_BEGIN(bool);

            while (!document.attributes_finished())
            {
                std::string key, value;
                MSS(document.pop_attribute(key, value), std::cerr << "Error getting attributes for Scenario" << std::endl);

                if (false)
                {
                }
                else if (key == "output_layout")
                    scenario.output_layouts.push_back(value);
                else if (key == "input_layout")
                    MSS(assign(scenario.input_layout, value));
                else if (key == "latency")
                    MSS(assign(scenario.latency, value));
                else
                    MSS(false, std::cerr << "Unknown attribute '" << key << "' for Scenario (output_layouts, input_layout and latency are allowed)" << std::endl);
            }

            bool initial_set = false;
            if (document.child_node_open())
            {
                while (!document.child_node_close())
                {
                    std::string tag;
                    MSS(document.pop_tag(tag), std::cerr << "Error getting tags for Scenario" << std::endl);

                    if (false)
                    {
                    }
                    else if (tag == "frame")
                        MSS(parse(append(scenario.frames), document));
                    else if (tag == "initial")
                    {
                        MSS(!initial_set);
                        MSS(parse(scenario.initial, document));
                        initial_set = true;
                    }
                    else
                        MSS(false, std::cerr << "Unknown tag '" << tag << "' for Scenario" << std::endl);
                }
            }

            MSS(initial_set, std::cerr << "Scenario:(initial) is required" << std::endl);

            MSS_END();
        }
    }

    bool load_scenario(std::istream &iss, Scenario &scenario)
    {
        MSS_BEGIN(bool);

        std::istreambuf_iterator<char> eos;
        std::string data(std::istreambuf_iterator<char>(iss), eos);

        bool settings_parsed = false;
        Document document(data);
        while (document.valid())
        {
            if (false)
            {
            }
            else if (document.pop_tag_if("scenario"))
                MSS(parse(scenario, document));
            else
                MSS(false);

            MSS(!document.valid());
        }

        auto prev = scenario.frames.begin();
        auto next = prev;
        next++;

        const auto &frames = scenario.frames;
        for (unsigned int i = 1; i < frames.size(); ++i)
        {
            MSS(frames[i - 1].index < frames[i].index, std::cerr << "Frame indices should be strict positive" << std::endl);
            ++next;
            ++prev;
        }

        MSS_END();
    }

}}
