#ifndef HEADER_app_gui_MainWindow_hpp_ALREADY_INCLUDED
#define HEADER_app_gui_MainWindow_hpp_ALREADY_INCLUDED

/*!
 * Paqart: Plugin Audio Quality Assurance and Regression Testing tool
 * Copyright (C) 2018  Leapwing Audio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <app/Options.hpp>
#include <app/PluginLoader.hpp>
#include "JuceHeader.h"

namespace app { namespace gui {

    class MainWindow : public juce::DocumentWindow, public juce::MenuBarModel
    {
    public:
        using PluginPtr = std::shared_ptr<juce::AudioPluginInstance>;

        enum FileEntries
        {
            Save = 1,
            Load = 2,
            ExportImage = 3,
        };

        MainWindow(bool is_visible = true)
            : juce::DocumentWindow("Paqart", juce::Colours::white, juce::DocumentWindow::allButtons, is_visible)
        {
            centreWithSize(500, 500);
            setVisible(is_visible);
            setResizable(true, true);
            setUsingNativeTitleBar(true);
            setMenuBar(this, 30);
        }

        ~MainWindow()
        {
            clearContentComponent();
            setMenuBar(nullptr);   
        }

        void run(const Options &options)
        {
            if (!run_(options))
                juce::JUCEApplication::getInstance()->systemRequestedQuit();
        }

        void closeButtonPressed() override
        {
            juce::JUCEApplication::getInstance()->systemRequestedQuit();
        }

        StringArray getMenuBarNames() override
        {
            return { "File" };
        };

        juce::PopupMenu getMenuForIndex(int toplevel_menu_index, const juce::String &name) override
        {
            auto res = juce::PopupMenu();

            res.addItem(FileEntries::Save, "save state");
            res.addItem(FileEntries::Load, "load state");
            res.addItem(FileEntries::ExportImage, "export image");

            return res;
        }

        void menuItemSelected(int menuItemID, int topLevelMenuIndex) override
        {
            switch (menuItemID)
            {
                case FileEntries::Save:
                    save_state_();
                    break;
                case FileEntries::Load:
                    load_state_();
                    break;
                case FileEntries::ExportImage:
                    export_image_();
                    break;

                default:
                    break;
            }
        }

    private:
        void save_state_()
        {
            auto fn = chooser_ ? chooser_->getResult() : juce::File{};
            chooser_ = std::make_unique<juce::FileChooser>("Save state", fn);
            const auto flags = juce::FileBrowserComponent::saveMode | juce::FileBrowserComponent::canSelectFiles | juce::FileBrowserComponent::warnAboutOverwriting;
            chooser_->launchAsync(flags, [this](const auto &chooser) {
                loader_.save_state(ptr_, chooser.getResult());
            });
        }

        void load_state_()
        {
            auto fn = chooser_ ? chooser_->getResult() : juce::File{};
            chooser_ = std::make_unique<juce::FileChooser>("Load state", fn);
            const auto flags = juce::FileBrowserComponent::openMode | juce::FileBrowserComponent::canSelectFiles;
            chooser_->launchAsync(flags, [this](const auto &chooser) {
                PluginLoader::load_state(ptr_, chooser.getResult());
            });
        }

        void export_image_()
        {
            auto fn = chooser_ ? chooser_->getResult() : juce::File{};
            chooser_ = std::make_unique<juce::FileChooser>("Export image", fn);
            const auto flags = juce::FileBrowserComponent::openMode | juce::FileBrowserComponent::canSelectFiles;
            chooser_->launchAsync(flags, [this](const auto &chooser) {
                auto w = this->getWidth();
                auto h = this->getHeight();
                auto pf = juce::Image::PixelFormat::ARGB;
                juce::Image img(pf, w, h, true);

                juce::Graphics ctx(img);
                paintEntireComponent(ctx, false);
                juce::FileOutputStream str(chooser.getResult());
                juce::PNGImageFormat().writeImageToStream(img, str);
            });
        }

        bool run_(const Options &options)
        {
            MSS_BEGIN(bool);
            MSS(loader_.initialize(options.plugin_filename));
            ptr_ = loader_.create(options.plugin_name, 48000, 2048);
            MSS(!!ptr_);
            ptr_->prepareToPlay(48000, 2048);

            auto *editor = ptr_->createEditorIfNeeded();
            MSS(!!editor);
            setContentOwned(editor, true);
            resized();

            MSS_END();
        }

        std::unique_ptr<juce::FileChooser> chooser_;
        PluginPtr ptr_;
        PluginLoader loader_;
    };

}}

#endif
