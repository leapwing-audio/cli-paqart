/*!
 * Paqart: Plugin Audio Quality Assurance and Regression Testing tool
 * Copyright (C) 2018  Leapwing Audio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef HEADER_app_Options_hpp_ALREADY_INCLUDED
#define HEADER_app_Options_hpp_ALREADY_INCLUDED

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include "JuceHeader.h"
#include "lw/Argo.hpp"
#include "lw/mss.hpp"

namespace app {

    struct Options
    {
        Options()
            : args_(configuration_())
        {
            using namespace lw::argo;

            args_.add(handler::Option{ "p", "--plugin-file", plugin_filename }.help("The audio processor plugin to load").nargs(1).required());
            args_.add(handler::Option{ "v", "--verbosity", verbosity }.help("The verbosity level (0 for silent)"));

            handler::group::Exclusive group("mode");
            {
                auto action = [&](lw::argo::core::Context &c) {
                    mode = Mode::Gui;
                    Arguments args(configuration_());
                    c.parser().merge(args);
                    return true;
                };
                group.add(handler::Flag("gui").help("").action(lw::argo::action::run(action)));
            }

            {
                auto action = [&](lw::argo::core::Context &c) {
                    mode = Mode::Headless;
                    Arguments args(configuration_());
                    args.add(handler::Option{ "i", "--input-file", input_file }.help("The input wave file").nargs(1).required());
                    args.add(handler::Option{ "o", "--output-file", output_filename }.help("The output wave file").nargs(1).required());
                    args.add(handler::Option{ "d", "--description-file", description_filename }.help("The description filename (naft format)").nargs(1).required());
                    args.add(handler::Option{ "s", "--plugin-state", plugin_state }.help("The file containing the plugin state (use the gui for creating these files)").optional());
                    c.parser().merge(args);
                    return true;
                };
                group.add(handler::Flag("headless").help("").action(lw::argo::action::run(action)));
            }
            args_.add(group);
        }

        lw::argo::Result parse(int argc, const char **argv)
        {
            return args_.parse(argc, argv);
        }

        bool help(std::ostream &oss) const
        {
            return args_.print_help(oss);
        }

        std::string input_file;
        std::string output_filename;
        std::string program_name;
        std::string description_filename;
        std::string plugin_filename;
        std::string plugin_name;
        std::string plugin_state;
        enum class Mode
        {
            Gui,
            Headless
        };
        Mode mode;
        unsigned int verbosity = 1;

    private:
        static lw::argo::Configuration configuration_()
        {
            lw::argo::Configuration conf;

            conf.program.name.brief = "Paqart";
            conf.program.name.extended = "Plugin Audio Quality Assurance and Regression Testing tool";
            conf.program.version.major = 0;
            conf.program.version.minor = 0;
            conf.program.version.patch = 1;

            return conf;
        }

        lw::argo::Arguments args_;
    };
}

#endif
