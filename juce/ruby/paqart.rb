require 'LW/executable'

module LW
    class Paqart

        attr_reader :name
        def initialize(mode: nil)
            @dir = File.dirname(File.dirname(__FILE__))
            @name = "Paqart"
            @mode = mode ? mode : :release
            @dependency = "core/public"
            @juce = Juce::Instance.new(@name, @dir)
            @juce.type = :console
        end

        def build(mode: nil)
            @mode = mode || @mode

            # configure the jucer
            @juce.mode = case @mode
                         when :release then "Release"
                         when :debug then "Debug"
                         else raise "Unknown mode #{mode}" end
            @juce.configure()

            cook = Cook::Instance.new([@mode])

            # Flatten the dependency
            tgt = Cook::Recipe.new()
            tgt.path = @dir
            cook.naft.find_all_dependencies(@dependency).each { |uri, rcp|  tgt.merge(rcp) }

            # insert into the jucer file
            src_fn = @juce.jucer_file(type: :template)
            dst_fn = @juce.jucer_file()

            LW::Juce.create_with_cook(src_fn, tgt, dst_fn)

            @exe = LW::Executable.new(@juce.build)
            @exe
        end

        def clean()
            @juce.clean
        end

        def execute(args)
            build unless @exe 
            @exe.execute(args)
        end
    end

    private
    def configure_(mode)
        @dir = File.dirname(File.dirname(__FILE__))
        @name = "Paqart"
        @mode = mode ? mode : :release
        @dependency = "core/public"
        @juce = Juce::Instance.new(@name, @dir)
        case OS.get
        when :mac
            @juce.primary_target = "#{@name}.app/Contents/MacOS/#{@name}"
        when :windows
            @juce.primary_target = "App/#{@name}.exe"
        end
    end

    def self.paqart()
        LW::Paqart.new()
    end
end
