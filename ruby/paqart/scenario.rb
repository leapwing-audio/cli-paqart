require 'LW/os'
require 'LW/wav/compare'

module LW
    class Paqart
        class Scenario
            attr_accessor :mode, :dir, :input, :plugin_fn, :prefix, :compare_function, :build_mode, :scenario_fn, :state_fn

            def self.default_scenario_fn
                "scenario.naft"
            end

            @@exe = nil

            def initialize(dir, input, plugin_fn, state_fn: nil, no_reference: false)
                @dir = dir
                @input = input
                @plugin_fn = plugin_fn
                @mode = :check
                @build_mode = :release
                @compare_function = ->(result) { result["RMS delta"] == 0 }
                @timestamp = Time.now.to_i
                @scenario_fn = File.join(dir, Scenario.default_scenario_fn)
                @state_fn = state_fn
                @no_reference = no_reference
            end

            def run
                rebuild unless @@exe
                raise "No scenario found '#{scenario_fn}'" unless File.exist? scenario_fn

                args = {
                    headless: nil,
                    d: scenario_fn,
                    i: @input,
                    p: @plugin_fn,
                    o: output_fn_(@no_reference ? :tmp : @mode),
                }

                if state_fn
                    raise "No state found '#{state_fn}'" unless File.exist? state_fn

                    args[:s] = state_fn
                end

                @@exe.execute(args)
            end

            def rebuild
                @@exe = LW.paqart
                @@exe.build(mode: build_mode)
            end

            def compare_generated_outputs(*os, threshold_dB: nil)
                my_os = OS.get
                kv_args = {}
                kv_args[:compare_value] = 10**(threshold_dB / 20.0) if threshold_dB

                os.select { |o| o != my_os }.each do |other|
                    my_os_file = output_fn_(:generate, os: my_os)
                    other_os_file = output_fn_(:generate, os: other)

                    LW::Wav.compare(my_os_file, other_os_file, **kv_args)
                end
            end

            def compare_with_reference(threshold_dB: nil)
                my_os_file = output_fn_(:generate)
                test_file = output_fn_(:check)

                kv_args = {}
                kv_args[:compare_value] = 10**(threshold_dB / 20.0) if threshold_dB
                LW::Wav.compare(my_os_file, test_file, **kv_args)
            end

            private

            def output_fn_(mode, os: nil)
                os ||= OS.get
                File.join(@dir, ["output", prefix, os, mode_to_str_(mode), "wav"].compact.map { |s| "#{s}" }.join("."))
            end

            def mode_to_str_(mode)
                case mode
                when :generate  then "reference"
                when :check     then "test"
                when :tmp       then "tmp"
                else raise "Unknown mode #{mode}"
                end
            end
        end
    end
end
