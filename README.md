# PAQART

PAQART is a Plugin Audio Quality Assurance and Regression Testing tool and is licensed under the LGPL v3 license (see [here](license.md)). 

## Requirements

 * PAQART is written in C++-14 
 * The script use ruby
 * Both juce and vst3sdk (see references) are used (but can be automatically retrieved)
 * For running scenarios, sox should be installed

## Building

The easiest way to build PAQART is as follows:
```
ruby build.rb 
```
However, this automatic configuration will download both SteinbergMedia vst3sdk and the juce sdk. Specifying and already installed sdk can be done as follows:
```
ruby build.rb --juce-dir <juce directory> --vst3sdk-dir <vst3sdk directory>
```

## Executing

to be added



## References

Paqart uses the following LGPL v3 projects. Make sure when using PAQART you respect their licenses.
 * [juce](https://github.com/WeAreROLI/JUCE)
 * [vst3sdk](ttps://github.com/steinbergmedia/vst3sdk)


![vst 3 sdk compliant](https://github.com/steinbergmedia/vst3_doc/raw/master/artwork/VST_Compatible_Logo_Steinberg.png)
![juce](http://www.brendev.co/assets/img/post-images/slider-images/juce-logo-slider.png)


